export class Usuario {
    constructor(
        public iduser: number,
        public nombre: string,
        public telefono: string,
        public email: string,
        public password:string,
        public activo : boolean,
        public perfil: number,
        public matricula?: string,
        public ape_p?: string,
        public ape_m?: string,
        public rfc?: string,
        public n_control?: string,
        public carrera?: string,
        public imagen?: string,
        public ubicacion?: string,
        public descripcion?: string,

        ){}
}